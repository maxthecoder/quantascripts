#!/bin/bash
########################################################################################################################
# Author: Daniel Ciulinaru
# Email:  daniel.ciulinaru@global.ntt
# backup_home:
# 	- THE MOTHERLOADE LOCATION, critical, needs to be set and RWX for the user executing the backups
# writestodisk:
# 	- YOU NEED TO SET THIS IF YOUR SCRIPT WILL GENERATE BACKUP PIECES THAT WILL GO TO THE {DB_NAME}/{FREQUENCY} FOLDER
# 	- determines if the dynamic folders will be created at run-time or not
# 	- should be: "yes" or "no"
# needscatalog:
# 	- YOU NEED TO SET THIS IF YOUR SCRIPT WILL USE CATALOG CONNECTION
#  	- determines if the catalog variables from the password file are being checked or not
# 	- should be: "yes" or "no"
# backup_type:
# 	- if "writestodisk" == "yes", "backup_type" has to be one of values:
# 		- level0, level1d, level1c, arc
# 	- if "writestodisk" == "no", "backup_type" has to be one of values:
# 		- backup, configure, crosscheck, delete, list, restore, validate
# frequency:
# 	- needs to take the values function of what "writestodisk" value is.
# 	- if "writestodisk" == "yes", "frequency" has to be:
# 		-	regular, monthly, yearly, adhoc, emergency, sweeper
# 	- if "writestodisk" == "no", "frequency" has to be:
# 		-	validate, clear, archivelog, backup, obsolete, preview, validate, database
# 	- "backup_type" and "frequency" determine the composite name of the RMAN run-time script, so pay attention!
# writetoregular:
# 	- an array that contains all the jobs that need to create backuppieces in the "regular" folder
# 	  that is, day to day backup jobs (not LTR, not adhoc)
# 	- when adding new types of jobs, make sure to also adjust the FrequencyDestTag pattern in the actual
#     RMAN template file
location="$(dirname $(/bin/readlink -f $0))";
backup_home="$($location/_get.backup_home.sh)" || \
if [ $? -ne 0 ]; then
	echo -e "Unable to correctly determine the backup_home";
	exit 1;
fi;
writestodisk="yes";
needscatalog="no";
backup_type="arc";
frequency="regular";
writetoregular=( "regular" "sweeper" "emergency" );
rman_type="${backup_type}.${frequency}";
guidtag="$(cat /proc/sys/kernel/random/uuid | tr --delete -)";

# decide which frequency type job lands into what folder:
decider=$(echo "${writetoregular[@]}" | grep -oE "(^|\s)$frequency($|\s)" | wc -l);
if [ "$writestodisk" == "yes" ] && [ "$decider" == 1 ]; then
	dest_folder="regular";
elif [ "$writestodisk" == "yes" ]; then
	dest_folder="$frequency";
fi;

# basic RMAN variables:
default_parallelism=4;
default_autobackup="on";
default_compression="medium";
default_optimization="off";
default_arcfilesperset=10;
default_backupfilesperset=1;
default_backedup_times=2;
default_sectionsize="8G";
# SRQ1473208: implement the archivelog deletion time range
default_until_time_hours="18";

# default recovery destination percentage used:
default_rdpu_threshold=40;

# Define default retention policies:
default_retention_regular=35;
default_retention_monthly=365;
default_retention_yearly=2555;

# cmdfile creation exit threshold: we attempt to create it for exit_threshold times, if not, exit:
exit_threshold=3

# scripts, logs folders names:
scripts_folder="scripts";
logs_folder="logs";
backup_home_scripts="${backup_home}/${scripts_folder}";
backup_home_logs="${backup_home}/${logs_folder}";

########################################################################################################################
# START COLLECTING AND CHECKING THE ARGUMENTS:
env_tag="$(${backup_home_scripts}/_validate.group.sh $1)" || \
if [ $? -ne 0 ]; then
	echo -e "${env_tag}";
	exit 1;
fi;

# Check if the incoming argument is passed correctly to the current script
if [ -z "$2" ]; then
	printf "%s\n" "";
	printf "%s\n" "The second argument cannot be empty";
	printf "%s\n" "The second argument needs to be the database name (not the SID), provided by the db_name parameter";
	exit 1;
else
	sid=${2///};
	# Check if the incoming argument exists in /etc/oratab
	inoratab=$(cat /etc/oratab | egrep -v "^#|ASM|MGMTDB|egrep" | egrep -v "^$" | cut -d ":" -f 1 | sort | uniq | grep -oE "(^|\s)$sid($|\s)" | wc -w)
	if [[ "$inoratab" -eq "0" ]]; then
		printf "%s\n" "";
		printf "%s\n" "$sid does not exist in /etc/oratab";
		exit 1;
	fi;
fi;

########################################################################################################################
# Set the environment
export PATH=/usr/local/bin:$PATH
export ORACLE_SID=$sid
oraenv=$(command -v oraenv 2>/dev/null) || if [ $? -ne 0 ]; then echo -e "oraenv could not be found"; exit 1; fi;
export ORAENV_ASK=NO
. "$oraenv" 2>/dev/null 1>/dev/null;
oracle_home="$ORACLE_HOME";
export NLS_LANGUAGE="AMERICAN_AMERICA.UTF8";
export NLS_DATE_FORMAT="DD-MON-YYYY HH24:MI:SS";
rman="${oracle_home}/bin/rman";

# define the db_name location:
db_name="$(${backup_home_scripts}/_get.db_name.sh $env_tag $sid)" || \
if [ $? -ne 0 ]; then
	echo -e "${db_name}";
	exit 1;
fi;
db_name="$(echo "$db_name" | tr '[:lower:]' '[:upper:]')";

# collect current timestamp, in different formats:
now="$(date +%Y%m%d%H%M%S)";
today="$(date +%Y_%m_%d)";
tomorrow="$(date +%Y_%m_%d -d 1day)";

# define logs location:
# define logfile:
# define run-time cmdfile:
backup_home_logs_today="${backup_home_logs}/${today}";
# verify that the scripts and logs locations exist:
mkdir -p "${backup_home_logs_today}" 2>/dev/null || \
if [ $? -ne 0 ]; then
	echo -e "\n";
	echo -e "Unable to create today logs folder: ${backup_home_logs_today}";
	echo -e "Exit";
	exit 1;
fi;
logfile="${backup_home_logs_today}/${db_name}.${rman_type}.${now}.log";
cmdfile="${backup_home_logs_today}/${db_name}.${rman_type}.${now}.rman";

# define finish function:
# 	- it will monitor exit codes and wrap up the run-time cmdfile and the logfile
finish() {
	local target="connect target";
  	local catalog="connect catalog";
  	sed -i "/$target/d;/$catalog/d" "${cmdfile}" 2>/dev/null;
  	gzip -9 "${cmdfile}" 2>/dev/null;
  	gzip -9 "${logfile}" 2>/dev/null;
}
trap finish ERR EXIT INT TERM SIGINT SIGQUIT SIGHUP SIGTERM;

########################################################################################################################
# define scripts location:
# define template RMAN file:

mkdir -p "${backup_home_scripts}" 2>/dev/null ||  \
if [ $? -ne 0 ]; then
	echo -e "\n" | tee -a "$logfile";
	echo -e "Unable to create ${scripts_folder} folder: ${backup_home_scripts}" | tee -a "$logfile";
	echo -e "Exit" | tee -a "$logfile";
	exit 1;
fi;
templatefile="${backup_home_scripts}/${rman_type}.rman";
if [ ! -f "${templatefile}" ]; then
	echo -e "\n" | tee -a "$logfile";
	echo -e "Unable to locate the template RMAN file: ${templatefile}" | tee -a "$logfile";
	echo -e "Exit" | tee -a "$logfile";
	exit 1;
fi;

# define db_name location:
#	- if "writestodisk" == "yes" that means any generated backuppiece is written here
#  	- "frequency" folder can take values: regulary, monthly, yearly
backup_home_databases="${backup_home}/databases";
backup_home_db_name="${backup_home_databases}/${db_name}";
mkdir -p "${backup_home_db_name}" 2>/dev/null || \
if [ $? -ne 0 ]; then
	echo -e "\n" | tee -a "$logfile";
	echo -e "Unable to create db_name folder: ${backup_home_db_name}" | tee -a "$logfile";
	echo -e "Exit" | tee -a "$logfile";
	exit 1;
fi;

# Create the dynamic folders, that depend on a db_name, "frequency", "today" and "tomorrow"
if [ "$writestodisk" == "yes" ]; then
	backup_home_db_name_backup_type="${backup_home_db_name}/${dest_folder}";
	mkdir -p "${backup_home_db_name_backup_type}/${today}" 2>/dev/null || \
	if [ $? -ne 0 ]; then
		echo -e "\n" | tee -a "$logfile";
		echo -e "Unable to create today's date folder: ${backup_home_db_name_backup_type}/${today}" | tee -a "$logfile";
		echo -e "Exit" | tee -a "$logfile";
		exit 1;
	fi;
	mkdir -p "${backup_home_db_name_backup_type}/${tomorrow}" 2>/dev/null || \
	if [ $? -ne 0 ]; then
		echo -e "\n" | tee -a "$logfile";
		echo -e "Unable to create tomorrow's date folder: ${backup_home_db_name_backup_type}/${tomorrow}" | tee -a "$logfile";
		echo -e "Exit" | tee -a "$logfile";
		exit 1;
	fi;
fi;

########################################################################################################################
# Define retention based on "frequency" of backup.
# 	The SOX case will be treated as an added argument to the command line
# 	The non-SOX retention is the default for yearly backups
echo -e "\n";
echo -e "\nCollecting Retention Settings";
case $frequency in
  	"regular" | "adhoc")
    	retention_tag=${3:-"${default_retention_regular}"};
    	echo -e "Frequency set to: 	${frequency}" | tee -a "$logfile";
    ;;

	"monthly")
		retention_tag=${3:-"${default_retention_monthly}"};
    	echo -e "Frequency set to: 	${frequency}" | tee -a "$logfile";
    ;;

	"yearly")
    	retention_tag=${3:-"${default_retention_yearly}"};
    	echo -e "Frequency set to: 	${frequency}" | tee -a "$logfile";
    ;;

  *)
    	retention_tag=${3:-"${default_retention_regular}"};
    	echo -e "Frequency set to: 	${frequency}" | tee -a "$logfile";
    ;;
esac;
echo -e "Retention set to:	${retention_tag}" | tee -a "$logfile";
echo -e "\n";

########################################################################################################################
# authentication: attempt to collect the password:
local_password_file="${backup_home_scripts}/.${env_tag}.password";
. "$local_password_file" 2>/dev/null || \
if [ $? -ne 0 ]; then
	echo -e "\n" | tee -a "$logfile" 2>/dev/null;
	echo -e "Unable to extract the $env_tag password." | tee -a "$logfile" 2>/dev/null;
	echo -e "The $env_tag password is supposed to be stored at: ${local_password_file}. Exit" | tee -a "$logfile" 2>/dev/null;
	exit 1;
fi;
# Checking the variables: username, password; if "needscatalog" == yes, check the associated variables as well:
if [ -z "$username" ]; then
	echo -e "\n" | tee -a "$logfile" 2>/dev/null;
	echo -e "The username variable has not been correctly declared in: ${local_password_file}" | tee -a "$logfile" 2>/dev/null;
	echo -e "It has to be in the format 'username=value', no quotes" | tee -a "$logfile" 2>/dev/null;
	echo -e "Exiting" | tee -a "$logfile" 2>/dev/null;
	exit 1;
fi;
if [ -z "$password" ]; then
	echo -e "\n" | tee -a "$logfile" 2>/dev/null;
	echo -e "The password variable has not been correctly declared in: ${local_password_file}" | tee -a "$logfile" 2>/dev/null;
	echo -e "It has to be in the format 'password=value', no quotes" | tee -a "$logfile" 2>/dev/null;
	echo -e "Exiting" | tee -a "$logfile" 2>/dev/null;
	exit 1;
fi;

# Check the variables for catalog credentials:
if [ "$needscatalog" == "yes" ]; then
	if [ -z "$rcat_u" ]; then
		echo -e "\n" | tee -a "$logfile" 2>/dev/null;
		echo -e "The rcat_u variable has not been correctly declared in: ${local_password_file}" | tee -a "$logfile" 2>/dev/null;
		echo -e "It has to be in the format 'rcat_u=value'" | tee -a "$logfile" 2>/dev/null;
		echo -e "Exiting" | tee -a "$logfile" 2>/dev/null;
		exit 1;
	fi;
	if [ -z "$rcat_p" ]; then
		echo -e "\n" | tee -a "$logfile" 2>/dev/null;
		echo -e "The rcat_p variable has not been correctly declared in: ${local_password_file}" | tee -a "$logfile" 2>/dev/null;
		echo -e "It has to be in the format 'rcat_p=value'" | tee -a "$logfile" 2>/dev/null;
		echo -e "Exiting" | tee -a "$logfile" 2>/dev/null;
		exit 1;
	fi;
	if [ -z "$rcat_db" ]; then
		echo -e "\n" | tee -a "$logfile" 2>/dev/null;
		echo -e "The rcat_db variable has not been correctly declared in: ${local_password_file}" | tee -a "$logfile" 2>/dev/null;
		echo -e "It has to be in the format 'rcat_db=value'" | tee -a "$logfile" 2>/dev/null;
		echo -e "Exiting" | tee -a "$logfile" 2>/dev/null;
		exit 1;
	fi;
fi;

# collect the template script in a shell variable so we could perform changes to it:
trap finish ERR EXIT INT TERM SIGINT SIGQUIT SIGHUP SIGTERM;
populate_runtime(){
	runtime="$(cat $templatefile)" || \
	if [ "$?" -ne 0 ] || [ -z "$runtime" ]; then
		echo -e "The runtime variable is empty: \"${runtime}\"" | tee -a "$logfile";
		echo -e "Unable to create the RMAN run-time variable in memory" | tee -a "$logfile";
		echo -e "Exit" | tee -a "$logfile" && exit 1;
	fi;
	# Overriding the defaults:
	# if you need to override these defaults, place these variables in the ".${env_tag}.password" file
	parallelism=${parallelism:=$default_parallelism};
	autobackup=${autobackup:=$default_autobackup};
	compression=${compression:=$default_compression};
	optimization=${optimization:=$default_optimization};
	arcfilesperset=${arcfilesperset:=$default_arcfilesperset};
	backupfilesperset=${backupfilesperset:=$default_backupfilesperset};
	backedup_times=${backedup_times:=$default_backedup_times};
	sectionsize=${sectionsize:=$default_sectionsize};
	rdpu_threshold=${rdpu_threshold:=$default_rdpu_threshold};

	if [ "$env_tag" == "dr" ]; then
		# when a DR db is worked on, remove the "configure retention" command,
		# 	because it is not supported on a physical standby
		retention_pattern="configure retention policy";
		resync_old_pattern="resync catalog;";
		resync_new_pattern="resync catalog from db_unique_name TargetTag;";
		runtime=$(echo "$runtime" | sed -e "/$retention_pattern/d");
		runtime="${runtime//$resync_old_pattern/$resync_new_pattern}";

        # SRQ1473208: deal with archivelog deletion requirements:
        # when the databases group being worked on is "dr", add a time clause to the archivelog delete statements
        # default is 18 hours, otherwise place a variable in the password file
        # example: until_times_hours=16
        until_times_hours="${until_times_hours:=$default_until_time_hours}";
        if ! [[ $until_times_hours -ge 0 ]]; then
            # validate that the extracted until_times_hours is an actual number
            echo "The following message concerns the number of hours the archivelogs need to be preserved on the +RECO disk" | tee -a "$logfile" 2>/dev/null;
            echo "Warning: until_times_hours is not a number. Setting it back to the default of $default_until_time_hours" | tee -a "$logfile" 2>/dev/null;
            until_times_hours="$default_until_time_hours/24";
        else
            # checks out as a number; test if it is a lower number than 24
            until_times_hours="$until_times_hours/24";
        fi
        # updating the archivelog deletion requirements:
        runtime="${runtime//all backed up BackeduptimesTag times/until time 'SYSDATE-($until_times_hours)' backed up BackeduptimesTag times}";
	fi;

	# update the username, password, db_name:
	# if it needs catalog, update catalog related variables
	runtime="${runtime//UsernameTag/$username}";
	runtime="${runtime//PasswordTag/$password}";
	runtime="${runtime//TargetTag/$sid}";
	runtime="${runtime//DatabaseTag/$db_name}";
	if [ "$needscatalog" == "yes" ]; then
	  	# catalog related:
		runtime="${runtime//RCATUTag/$rcat_u}";
		runtime="${runtime//RCATPTag/$rcat_p}";
		runtime="${runtime//RCATDBTag/$rcat_db}";
	else
		# When the catalog is not used, ensure that the "connect catalog" is removed from the runtime variable:
		runtime=$(echo "$runtime" | sed -e "/connect catalog/d;/resync catalog/d");
	fi;

	# in an emergency situation, reduce the number after how many backups an archivelog can be deleted:
	if [ "$backup_type" == "arc" ] && [[ "$frequency" =~ ^(emergency|sweeper)$ ]]; then
		backedup_times=1;
	fi;

	# When doing LTR backups (monthly/yearly/adhoc), turn off the controlfile autobackup
	# 	it will be automatically turned on by any other regular backup:
	# 	The LTR backups cannot have a tag associated with a controlfile autobackup, therefore the autobackup is useless
	# 	for the purpose of long term preservation via "change backupset tag..."
	if [[ "$frequency" =~ ^(monthly|yearly|adhoc|emergency|sweeper)$ ]]; then
		autobackup="off";
	fi;
	# if for any reason the controlfile autobackup has been turned off,
	# also remove the autobackup configuration statement:
	if [[ "$autobackup" == "off" ]]; then
		remove_autobackup="configure controlfile autobackup format for device type";
		runtime=$(echo "$runtime" | sed -e "/$remove_autobackup/d");
	fi;

	# update the rest of the variables
	runtime="${runtime//guidtag/$guidtag}";
	runtime="${runtime//TimeTag/$now}";
	runtime="${runtime//RetentionTag/$retention_tag}";
	runtime="${runtime//FrequencyTag/$frequency}";
	runtime="${runtime//FrequencyDestTag/$dest_folder}";
	runtime="${runtime//ArcFilespersetTag/$arcfilesperset}";
	runtime="${runtime//BackupFilespersetTag/$backupfilesperset}";
	runtime="${runtime//BackeduptimesTag/$backedup_times}";
	runtime="${runtime//SectionsizeTag/$sectionsize}";
	runtime="${runtime//BackupTypeTag/$backup_type}";
	runtime="${runtime//BackupOptimizationTag/$optimization}";
	runtime="${runtime//ParallelismTag/$parallelism}";
	runtime="${runtime//CompressionTag/$compression}";
	runtime="${runtime//BackupDestinationTag/$backup_home_databases}";
	runtime="${runtime//ControlfileAutobackupTag/$autobackup}";
}

create_cmdfile(){
	# Check that the run-time RMAN script file really exists. The while loop is required because of: IRQ2194852
	# Exit if it meets the $exit_threshold number of failures:
	counter=0;
	while [ ! -f "$cmdfile" ]
	do
		counter=$(( counter + 1 ))
		echo -e "Attempt #${counter} to create the run-time RMAN script: ${cmdfile}" | tee -a "$logfile";
		echo -e "${runtime}">"${cmdfile}" 2>/dev/null || \
		if [ $? -ne 0 ]; then
			echo -e "Unable to create the RMAN run-time script: ${cmdfile}" | tee -a "$logfile";
			echo -e "Exit" | tee -a "$logfile";
			exit 1;
		fi;
		sleep 1;
		if [ $counter -eq $exit_threshold ]; then
			echo -e "We tried ${counter} times to create the $cmdfile script and ended in a miserable failure!!!" | tee -a "$logfile";
			echo -e "Exiting, tail between the legs." | tee -a "$logfile";
			exit 1;
		fi;
	done;
}

########################################################################################################################
# define launch:
# launch the launch:
launch() {
	set -o pipefail;
	("$rman" cmdfile="$cmdfile" 2>&1) | tee -a "$logfile" &
	child_process=$!;
	trap finish ERR EXIT INT TERM SIGINT SIGQUIT SIGHUP SIGTERM;
	wait $child_process;
	exit_code=$?;

	# update the status file:
  	start_time="$now";
	end_time=$(date +%Y%m%d%H%M%S);
	"${backup_home_scripts}/update.status.sh" "${guidtag}" "${db_name}" "${rman_type}" "${start_time}" "${end_time}" "${exit_code}" "${logfile}";
}

populate_runtime;
create_cmdfile;
launch;
exit "$exit_code";