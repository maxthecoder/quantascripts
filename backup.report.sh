#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------------------
# 02/02/2017 - updated script to preclude ODRSAM11 - IRQ0874563.
# 02/02/2017 - updated scripts CONTENT variable to use full file path to fix OEM sch job mv failure.
# 03/30/2017 - updated script to send email to ticketing system if count is off.
# 04/10/2017 - updated two values and service now alert functionality.
# 05/23/2017 - updated script to include an output of missing databases not backed up, also added delete command but commented out for now.
# 06/28/2017 - I have disabled/removed the backup(for 1week) JDSBAMBD against SRQ0590169 ​.
# 07/19/2017 - SRQ0614072 Oracle-Remove database from OEM jobs and blackout indefinately - JDSBAMSY
# 07/19/2017 - SRQ0590169 Oracle-Remove database instance from OEM jobs and blackout for 1 week – JDSBAMBD.
# 07/21/2017 - Request to add database(JDSBAMBD) back into the report.
# 07/26/2017 - CHGT0161874 remove JDCVAMBD from report
# 08/21/2017 - Kim Vo has requested that ODPDAM12 not be backed up at this time.
# 20200421 	 - DC: rewritten to be included with the backup scripts and take advantage of the authentication single point
#---------------------------------------------------------------------------------------------------------------------------------------------
backup_home="/qs2nfsqa01/RMAN_QA/backup"
scripts_folder="scripts"
now="$(date +%Y%m%d%H%M%S)"
env_tag="dv"
backup_home_scripts="${backup_home}/${scripts_folder}"

# get the authentication info:
local_password_file="${backup_home_scripts}/.${env_tag}.password"
. "$local_password_file" 2>/dev/null ||
	if [ $? -ne 0 ]; then
		echo -e "\n" 2>/dev/null
		echo -e "Unable to extract the $env_tag password." 2>/dev/null
		echo -e "The $env_tag password is supposed to be stored at: ${local_password_file}. Exit" 2>/dev/null
		exit 1
	fi

# validate variables: rcat_u, rcat_p, rcat_db
if [ -z "$rcat_u" ]; then
	echo -e "\n" 2>/dev/null
	echo -e "The username variable has not been correctly declared in: ${local_password_file}" 2>/dev/null
	echo -e "It has to be in the format 'username=value', no quotes" 2>/dev/null
	echo -e "Exiting" 2>/dev/null
	exit 1
fi
if [ -z "$rcat_p" ]; then
	echo -e "\n" 2>/dev/null
	echo -e "The password variable has not been correctly declared in: ${local_password_file}" 2>/dev/null
	echo -e "It has to be in the format 'password=value', no quotes" 2>/dev/null
	echo -e "Exiting" 2>/dev/null
	exit 1
fi
if [ -z "$rcat_db" ]; then
	echo -e "\n" 2>/dev/null
	echo -e "The catalog database variable has not been correctly declared in: ${local_password_file}" 2>/dev/null
	echo -e "It has to be in the format 'password=value', no quotes" 2>/dev/null
	echo -e "Exiting" 2>/dev/null
	exit 1
fi

export ORADATE="$now"
export ORACLE_SID="$rcat_db"
export ORAENV_ASK=NO
. oraenv >/dev/null
sqlplus="${ORACLE_HOME}/bin/sqlplus"

# functions section:
fn_echo_time() {
	date +%Y%m%dT%H%M%S%Z
}

fn_print() {
	# fn_print is used inside other functions to print the message along with a timestamp and unify the displaying format;
	# do not use it independently;
	for i in "$@"; do
		echo -e "$(fn_echo_time)\t${!1}"
		shift
	done
}

fn_result() {
	local _function_name
	_function_name=$(basename ${FUNCNAME[0]})
	if [[ -z "${!1}" ]]; then
		local message="$_function_name: needs a valid argument to display"
		fn_print message
		exit 1
	fi
	# Sanitizing returns from functions
	echo -e "${!1}"
}

fn_sqlplus() {
	# SQLPLUS wrapper, used by other functions, for expediency and consistency
	# makes a generic connection to the database
	# expects 5 local parameters:
	# 	dbuser 			- username to connect to the Oracle database
	# 	dbupass 		- password to connect to the Oracle database
	# 	dbucat 			- db_name
	#	sqlscript		- the actual sql command
	#	formatscript 	- to format the output
	# returns the result to the calling function
	local dbuser="${!1}";
	local dbpass="${!2}";
	local dbcat="${!3}";
	local sql="${!4}";
	local format="${!5}";

	check_sys="$(echo ${dbuser} | tr '[:upper:]' '[:lower:]')"
	local connection="";
	local result;

	connstring="$dbuser/\"$dbpass\"@$dbcat"
	if [ "$check_sys" == "sys" ]; then
		connection=$connstring" as sysdba"
	else
		connection=$connstring
	fi

	echo -e "$sql";
	set -xv
	result=$(
		$sqlplus -S -L "$connection" <<EOF
$format
$sql
/
EOF
	)
	printf "%s" "$result";
}

fn_static_count() {
	local formatting;
	local sqlscript;
	local check;
	local result;

	formatting="set feedback off heading off"
	sqlscript="
select 	count(at1.DB_NAME)
from 	RCAT2.RC_RESYNC at1
inner join (
	select	db_name
			,max(resync_time) as maxdate
	from 	rcat2.rc_resync
	group by
			db_name) at2
ON
		at1.db_name = at2.db_name and at1.resync_time = at2.maxdate
where   at1.db_name not in ('ODPDAM12','JDUA90BD','AWRPD01','ESSBAM01','ESPYAM02','JDPDAM88',
		'JDSXAMBD','JDPYAMDB','IPPDAM88','IPPDAM01','IPDVAM88','ODRSAM11','AEDVAM01','ESQAAM01','OE13PD01',
		'JDUA92BD','ESDVAM01','ESPYAM01','IPMGDEV','MGMT','MGMT1','UKDRAM01','UKPDAM01','DSDVAM02','HYDVAM01')
		order by at1.db_name;"
	result=$(fn_sqlplus rcat_u rcat_p rcat_db sqlscript formatting);
	check="$(echo -e "${result}" | tr -d '[:space:]')"
	if [[ "$check" =~ .*"ORA-".* ]]; then
		local message="Unable to retrieve the static count of databases in catalog";
		fn_print message result;
		exit 1
	fi
	echo -e "$result";
}
static_result=$(fn_static_count) ||
	if [ $? -ne 0 ]; then
		printf '%s\n' "$static_result"
		exit 1
	fi
echo -e "$static_result";