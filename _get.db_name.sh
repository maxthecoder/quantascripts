#!/bin/bash
########################################################################################################################
# Author: Daniel Ciulinaru
# Email:  daniel.ciulinaru@global.ntt
# Purpose: extract the DB_NAME parameter and pass it onto the calling script

# variables:
location="$(dirname $(/bin/readlink -f $0))"
backup_home="$($location/_get.backup_home.sh)" ||
  if [ $? -ne 0 ]; then
    echo -e "Unable to correctly determine the backup_home"
    exit 1
  fi
scripts_folder="scripts"
backup_home_scripts="${backup_home}/${scripts_folder}"

# get the life cycle status keyword:
env_tag="$(${backup_home_scripts}/_validate.group.sh $1)" ||
  if [ $? -ne 0 ]; then
    echo -e "${env_tag}"
    exit 1
  fi

# get the db/sid,
if [ -z "$2" ]; then
  printf "%s\n" ""
  printf "%s\n" "The second argument cannot be empty"
  printf "%s\n" "The second argument needs to be the database name (not the SID), provided by the db_name parameter"
  exit 1
else
  sid=${2///}
  # Check if the incoming argument exists in /etc/oratab
  inoratab=$(cat /etc/oratab | egrep -v "^#|ASM|MGMTDB|egrep" | egrep -v "^$" | cut -d ":" -f 1 | sort | uniq | grep -oE "(^|\s)$sid($|\s)" | wc -w)
  if [[ "$inoratab" -eq "0" ]]; then
    printf "%s\n" ""
    printf "%s\n" "$sid does not exist in /etc/oratab"
    exit 1
  fi
fi

# Set the environment
export PATH=/usr/local/bin:$PATH
export ORACLE_SID=$sid
oraenv=$(command -v oraenv 2>/dev/null) || if [ $? -ne 0 ]; then
  echo -e "oraenv could not be found"
  exit 1
fi
export ORAENV_ASK=NO
. "$oraenv" 2>/dev/null 1>/dev/null
oracle_home="$ORACLE_HOME"
export NLS_LANGUAGE="AMERICAN_AMERICA.UTF8"
export NLS_DATE_FORMAT="DD-MON-YYYY HH24:MI:SS"
sqlplus="${oracle_home}/bin/sqlplus"

# get the authentification info:
local_password_file="${backup_home_scripts}/.${env_tag}.password"
. "$local_password_file" 2>/dev/null ||
  if [ $? -ne 0 ]; then
    echo -e "\n" 2>/dev/null
    echo -e "Unable to extract the $env_tag password." 2>/dev/null
    echo -e "The $env_tag password is supposed to be stored at: ${local_password_file}. Exit" 2>/dev/null
    exit 1
  fi
# Checking the variables: username, password; if "needscatalog" == yes, check the associated variables as well:
if [ -z "$username" ]; then
  echo -e "\n" 2>/dev/null
  echo -e "The username variable has not been correctly declared in: ${local_password_file}" 2>/dev/null
  echo -e "It has to be in the format 'username=value', no quotes" 2>/dev/null
  echo -e "Exiting" 2>/dev/null
  exit 1
fi
if [ -z "$password" ]; then
  echo -e "\n" 2>/dev/null
  echo -e "The password variable has not been correctly declared in: ${local_password_file}" 2>/dev/null
  echo -e "It has to be in the format 'password=value', no quotes" 2>/dev/null
  echo -e "Exiting" 2>/dev/null
  exit 1
fi

# define functions:
fn_db_name() {
  # SQLPLUS wrapper, used by other functions, for expediency and consistency
  # makes a generic connection to the database
  # expects 2 local parameters:
  #	formatscript 	- to format the output
  #	sqlscript		- the actual sql command
  # returns the result to the calling function
  local formatscript="
set linesize 200 embedded on tab off pagesize 0 head off feed off echo off
col SCN format a20"
  local sqlscript="select value from v\$system_parameter2 where name='db_name'"

  if [ "$username" == "SYS" ] || [ "$username" == "sys" ]; then
    connection=$username'/"'$password'"@'$sid' as sysdba'
  else
    connection=$username'/"'$password'"@'$sid
  fi

  local result="$(
    $sqlplus -S -L "$connection" <<EOF
$formatscript;
$sqlscript
/
EOF
  )"
  local error_check
  error_check="$(echo -e "${result}" | tr -d '[[:space:]]')"
  if [[ "$error_check" =~ .*"ORA-".* ]]; then
    echo -e "\nUnable to retrieve db_name parameter:"
    echo -e "\nConnection used: $connection"
    echo -e "$result"
    exit 1
  fi
  printf "%s" "$result"
}
fn_db_name