#!/bin/bash
########################################################################################################################
# Author: Daniel Ciulinaru
# Email:  daniel.ciulinaru@global.ntt
location="$(dirname $(/bin/readlink -f $0))";
backup_home="$($location/_get.backup_home.sh)" || \
if [ $? -ne 0 ]; then
	echo -e "Unable to correctly determine the backup_home";
	exit 1;
fi;
status_file="${backup_home}/status.log";


########################################################################################################################
# START COLLECTING AND CHECKING THE ARGUMENTS:
# <guidtag>
if [ -z "$1" ]; then
	printf "%s\n" "";
	printf "%s\n" "Argument #1 is a guidtag and it needs to be populated.";
	printf "%s\n" " Currently is empty. Exit.";
	exit 1;
else
	guidtag="${1//-}";
fi;

# <db_name>
if [ -z "$2" ]; then
	printf "%s\n" "";
	printf "%s\n" "Argument #2 is a db_name tag and it needs to be populated.";
	printf "%s\n" "Currently is empty. Exit.";
	exit 1;
else
	db_name="${2///}";
fi;

# <backup_type>
if [ -z "$3" ]; then
	printf "%s\n" "";
	printf "%s\n" "Argument #3 is a backup_type tag and it needs to be populated.";
	printf "%s\n" "Currently is empty. Exit.";
	exit 1;
else
	backup_type="${3}";
fi;

# <start_time>
if [ -z "$4" ]; then
	printf "%s\n" "";
	printf "%s\n" "Argument #5 is a start_time tag and it needs to be populated.";
	printf "%s\n" "Currently is empty. Exit.";
	exit 1;
else
	start_time="${4}";
fi;

# <end_time>
if [ -z "$5" ]; then
	printf "%s\n" "";
	printf "%s\n" "Argument #6 is an end_time tag and it needs to be populated.";
	printf "%s\n" "Currently is empty, autopopulating with the current timestamp";
	exit 1;
else
	end_time="${5}";
fi;

# <exit_code>
if [ -z "$6" ]; then
	printf "%s\n" "";
	printf "%s\n" "Argument #7 is an exit_code tag and it needs to be populated.";
	printf "%s\n" "Currently is empty. Exit.";
	exit 1;
else
	exit_code="${6}";
fi;

# <logfile>
if [ -z "$7" ]; then
	printf "%s\n" "";
	printf "%s\n" "Argument #8 is a logfile tag and it needs to be populated.";
	printf "%s\n" "Currently is empty. Exit.";
	exit 1;
else
	logfile="${7}";
fi;

update_status()
{
        if [ ! -f "$status_file" ];
        then
                touch "$status_file";
        fi
        if [ "$exit_code" -gt 0 ];
        then
                exit_status='fail';
        elif [ "$exit_code" -eq 0 ];
        then
                exit_status='success';
        fi
        local status_line="guidtag: $guidtag|db_name: $db_name|backup_type: $backup_type|start_time: $start_time|end_time: $end_time|exit_status: $exit_status|log_file: $logfile";
        echo -e "$status_line";
        echo "$status_line" >>"$status_file";
}
update_status;