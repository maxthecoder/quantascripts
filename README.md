# How to deploy:

1. Make sure you have a proper NFS share/mount point, provided by the client;
that is, it needs to be read-writeable by linux user owner of the pmon processes

2. This NFS share will be named from here on: "backup_home"

3. Copy all scripts (shell wrappers + rman scripts) under:
    "backup_home"/"scripts"

4. Make sure that "backup_home"/"scripts" is owned by oracle

5. In all shell wrappers adjust the "backup_home" variable to point to the NFS
share provided at #1. Make sure there is no trailing slash appended to the path.
Also make sure that the path is enclosed in double quotes.

6. Under the "backup_home"/"scripts" path, create password files, as following:

    - for the DV databases, create a file called ".dv.password"
    - for the SB databases, create a file called ".sb.password"
    - for the QA databases, create a file called ".qa.password"
    - for the PY databases, create a file called ".py.password"
    - for the DR databases, create a file called ".dr.password"  
    
    Note: change the password files permissions to "600"    

7. In each of these password files add/update the following variables names
and values:

    Note: leave the comment about the single quotes in place, for future refence

    ```
    # if any username or password contains a special characters ($, # and such)  
    # please encase your value with single quotes
    username=\<THE RMAN BACKUP USER; has to have SYSDBA privileges\>  
    password=\<THE RMAN BACKUP PASSWORD\>  
    rcat_u=\<THE RMAN CATALOG USER\>  
    rcat_p=\<THE RMAN CATALOG PASSWORD\>  
    rcat_db=\<THE RMAN CATALOG TNS ALIAS\>
   
   # override default values:
    parallelism=6
    optimization="off"
    autobackup="on"
    compression="medium"
    arcfilesperset="10"
    backupfilesperset="1"
    sectionsize="8G"
    backedup_times=4
    rdpu_threshold=74
    ```

8. Make sure that the RAC TNS alias exists in the /etc/oratab, and correctly
associated with the Oracle home location of its instances.
    Note: - the scripts have been designed to be called with a RAC TNS alias,
            not with a SID that has a running pmon process.
          - that is, the RAC TNS alias will mandatory use a SCAN listener
            as a point of entry for authentication

9. Test one of the scripts with one of the databases that belongs to the 
DV/SB/QA/PY/DR groups. These groups are supposed to be defined in OEM.

10. Retentions break down, per Quanta's request:

    ```
    standard retention = 35 days
    monthly backups retention = 365 days
    yearly non-SOX retention = 730 days (2 years)
    yearly SOX retention = 2555 days (7 years)
    ```
Note: as of 10JUL2020, there is no more differentiation between LTR SOX and non-SOX retention.
All LTR yearly backups are retain automatically for 7 years.

11. The usage of scripts:  
    Note:   at the very minimum, each script has to have:  
            - two-letter 1st argument, reflecting the environment (see #6)  
            - the RAC tns alias used for authentication (see #8)  
    Note:   some scripts will need a 3rd argument, typically reflecting a  
            retention number of days change from the default
            
    - archivelog backup, standard retention:  
        `./arc.regular.sh dv hydvam02`
    
    - archivelog backup, with modified retention policy:  
        `./arc.regular.sh dv hydvam02 50`

    - level0 backup, standard retention:  
        `./level0.regular.sh qa hyqaam01`
    
    - level0 backup, modified retention policy:  
        `./level0.regular.sh qa hyqaam01 60`

    Note:   the regular backups, with a modified retention, will change the actual  
            retention policy, which applies to ALL regular backups but it does NOT apply  
            to backup that are marked, independently, with their own specific retention.  
            This is an RMAN specific mechanism.

    - level0 adhoc backup, standard retention:  
        `./level0.adhoc.sh dv hydvam02`
    
    - level0 adhoc backup, modified retention:  
        `./level0.adhoc.sh dv hydvam02 100`

    Note:   the adhoc, monthly, yearly backups, when called with a standard retention,  
            they mark the individual backuppieces themselves with the number of days to be  
            retained for; the scripts do NOT change the existing retention policy.  
            This approach is on purpose.  

    Note:   the adhoc, monthly and yearly backups, when called with a modified retention  
            (3rd argument present), the mark the individual backuppieces themselves with  
            the number of days to be retained for; the scripts do NOT change the existing  
            retention policy. This approach is on purpose.  

12. Explanation of purpose for each type of script:

    - arc.emergency.sh:  
        - emergency archivelog backup script, with a reduced
        number of configure statements, and not controlfile autobackup
        - it has to be run manually
        - it does NOT need a catalog connection  

    - arc.regular.sh:
        - regular archivelog backup script, with a standard number of configure
        statements
        - it can be run either manually or scheduled
        - it does NOT need a catalog connection
    
    - arc.sweeper.sh:
        - emergency archivelog backup script, with a reduced number of configure
        statements, and not controlfile autobackup
        - it is mandatory to run on scheduled basis, in a very short 
        cycle (5-15 minutes)
        - it does NOT need a catalog connection
        
    - backup.validate.sh:
        - it runs "the backup validate" on the database
        - it needs to run only manually, not scheduled
        - it does NOT need a catalog connection
    
    - configure.clear.sh:
        - it resets to default all the configure values
        - it needs to run just on need-basis
        - it DOES need a catalog connection

    - crosscheck.archivelog.sh:
        - it performs a crosscheck on all the archivelogs
        - it runs a "delete expired" on all the "FAILED" archivelogs, that is 
        archivelogs that were supposed to be present and found to be missing
        - it DOES need a catalog connection
        - it needs to run just on need-basis

    - crosscheck.backup.sh:
        - it performs a crosscheck on all the backuppieces types
        - it runs a "delete expired" on all the "FAILED" backuppieces, that is 
        backuppieces that were supposed to be present and found to be missing
        - it DOES need a catalog connection
        - it needs to run just on need-basis

    - delete.obsolete.sh:
        - it deletes all the backuppieces older than the prescribed standard
        retention policy (in Quanta's case, 35 days)
        - it can run either manually or scheduled
        - it DOES need a catalog connection
        
    - level0.adhoc.sh:
        - it performs a level0 backup of the database
        - it marks the generated backuppieces, individually, with the retention
        number of days, via the "change backupset tag" command
        - it does NOT change the standard retention policy. This is on purpose
        - if the 3rd argument is present, it supersedes the standard retention
        number of days
        - it does NOT need a catalog connection
        
    - level0.monthly.sh:
        - it performs a level0 backup of the database
        - it marks the generated backuppieces, individually, with the retention
        number of days, via the "change backupset tag" command
        - the default retention number for this type of backup is the
        "monthly backups retention" number of days
        - it does NOT change the standard retention policy. This is on purpose
        - if the 3rd argument is present, it supersedes the
        "monthly backups retention" number of days
        - it does NOT need a catalog connection
    
    - level0.regular.sh:
        - it performs a level0 backup of the database
        - it enforces the retention policy via the "standard retention" number
        - the default retention number for this type of backup is the
        "standard retention" number of days
        - it does enforce the standard retention policy. This is on purpose
        - if the 3rd argument is present, it supersedes the
        "standard retention" number of days and it changes the retention policy.
        This is on purpose
        - it does NOT need a catalog connection
        
    - level0.yearly.sh:
        - it performs a level0 backup of the database
        - it marks the generated backuppieces, individually, with the retention
        number of days, via the "change backupset tag" command
        - the default retention number for this type of backup is the
        "yearly non-SOX retention" number of days
        - it does NOT change the standard retention policy. This is on purpose
        - if the 3rd argument is present, it supersedes the
        "yearly non-SOX retention" number of days
        - it does NOT need a catalog connection
        
    - level1c.regular.sh:
        - it performs a level1 cumulative backup of the database
        - it enforces the retention policy via the "standard retention" number
        - the default retention number for this type of backup is the
        "standard retention" number of days
        - it does enforce the standard retention policy. This is on purpose
        - if the 3rd argument is present, it supersedes the
        "standard retention" number of days and it changes the retention policy.
        This is on purpose
        - it does NOT need a catalog connection
        
    - level1d.regular.sh:
        - it performs a level1 differential backup of the database
        - it enforces the retention policy via the "standard retention" number
        - the default retention number for this type of backup is the
        "standard retention" number of days
        - it does enforce the standard retention policy. This is on purpose
        - if the 3rd argument is present, it supersedes the
        "standard retention" number of days and it changes the retention policy.
        This is on purpose
        - it does NOT need a catalog connection
        
    - list.backup.sh:
        - it collects the current timestamp at run-time and uses that timestamp
        to find all the database backups performed up until that moment
        - it displays output for both "list backup" and "list backup summary"
        - it DOES need a catalog connection
        
    - register.database.sh:
        - it DOES need a catalog connection
        - it registers the database into the RMAN catalog
        - all the prerequisites must be completed manually, they are NOT handled
        automatically (check if the database has been registered before etc)
        - it runs ONLY on manual basis; do not schedule

    - restore.preview.sh:
        - it collects the current timestamp at run-time and uses that timestamp
        to determine what backuppieces would be necessary to restore and
        recover the database up to the specified timestamp
        - it does NOT need a catalog connection
        - useful for what-if scenarios
        - it runs on manual basis

    - restore.validate.sh:
        - it collects the current timestamp at run-time and uses that timestamp
        to determine if the backuppieces necessary for the restore and recovery
        of the database and its accompanying archivelogs are present and valid
        - it does NOT need a catalog connection
        - it runs on either manual or scheduled basis
        
    - resync.catalog.sh:
        - it resyncs the catalog for the database specified as a 2nd argument
        - it DOES need a catalog connection
        - it can run both as a manual or a scheduled operation
        
    - validate.database.sh:
        - it performes a "validate database"
        - it does NOT need a catalog connection
        - it only should run as a manual operation
