#!/bin/bash
########################################################################################################################
# Author: Daniel Ciulinaru
# Email:  daniel.ciulinaru@global.ntt
# Purpose: validate the group names: DV, PY, SB, QA, DR, PD

# validate the life cycle status keyword:
env_tag="$(echo "$1" | tr '[:upper:]' '[:lower:]')";
if [ -z "$1" ] || [[ ! "$env_tag" =~ ^(dv|py|sb|qa|dr|pd)$ ]]; then
	printf "%s\n" "";
	printf "%s\n" "The environment two-letter keyword is not in the accepted list: DV, PY, SB, QA, DR, PD";
	printf "%s\n" "Exiting.";
	exit 1
else
	printf "%s" "$env_tag";
fi;
exit 0;