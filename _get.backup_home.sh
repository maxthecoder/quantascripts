#!/bin/bash
########################################################################################################################
# Author: Daniel Ciulinaru
# Email:  daniel.ciulinaru@global.ntt
# Purpose: one script that provides a hardcoded $backup_home location to all calling scripts.
# The $backup_home needs not to be determined dynamically.

# declare backup_home:
backup_home="/qs2nfsdr01/RMAN_DR/backup";
printf "%s" "$backup_home";
exit 0;